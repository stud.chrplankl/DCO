package actor

sealed  trait Message

sealed case class Greeting(message: String)
sealed case class GreetingReceived(reply: String)


sealed trait SupervisionMessages


sealed case class ResumeException(msg: String) extends Exception(msg) with  SupervisionMessages
sealed case class RestartException(msg: String) extends Exception(msg) with SupervisionMessages
sealed case class StopException(msg: String) extends Exception(msg) with SupervisionMessages
sealed case class EscalateExcetption(msg: String) extends Exception(msg) with SupervisionMessages
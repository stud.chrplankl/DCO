package actor
import akka.persistence
import akka.persistence.{PersistentActor, SnapshotOffer}
import message._;


case class CommandActorState(var currentlyStoredCommands: List[String], var currentlyStoredProtocols: List[String]){
  //var currentlyStoredCommand: String = _
  //var currentlyStoredEvent: String = _
  override def toString: String = {
    "StoredCommands: \n" + currentlyStoredCommands.map(x=>x.toString +" \n") + "\nStoredProtocols: \n" + currentlyStoredProtocols.map(x=>x.toString + "\n")
  }
}

class CommandActor(id: String) extends PersistentActor {

  override def persistenceId: String = id

  var myState = CommandActorState(List("init"), List("init"))


  val execute: RecoveryMessage => Unit= {
    case CommandReceivedMessage(msg) => myState.currentlyStoredCommands = msg::myState.currentlyStoredCommands
    case ProtocolReceivedMessage(msg) => myState.currentlyStoredProtocols = msg::myState.currentlyStoredProtocols
  }


  override def receiveCommand: Receive = {
    case CommandMessage(msg) => {
      persist(CommandReceivedMessage(msg))(execute)
      if((myState.currentlyStoredCommands.length+myState.currentlyStoredProtocols.length) % 10 == 0){
        println("save snapshot")
        saveSnapshot(myState)
      }
    }
    case ProtocolMessage(msg) =>{
      persist(ProtocolReceivedMessage(msg))(execute)
      if((myState.currentlyStoredCommands.length+myState.currentlyStoredProtocols.length) % 10 == 0){
        println("save snapshot")
        saveSnapshot(myState)
      }
    }
    case "print" => {
      println(myState.currentlyStoredProtocols.length)
      println(myState.currentlyStoredCommands.length)
      println(myState.toString)
    }
  }

  override def receiveRecover: Receive = {
    case message @ ProtocolReceivedMessage(msg) => execute(message)
    case message @ CommandReceivedMessage(msg) => execute(message)
    case SnapshotOffer(_, snapshot: CommandActorState) => myState = snapshot
  }
}

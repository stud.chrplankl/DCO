package actor

import akka.actor.{Actor, OneForOneStrategy, Props, SupervisorStrategy}
import java.util.UUID.randomUUID
import scala.concurrent.duration._
import akka.actor.SupervisorStrategy.{Escalate, Restart, Resume, Stop}


class FirstActor extends Actor {

  val toActorResume = context.actorOf(Props[ToActor], name = randomUUID().toString)
  val toActorRestart = context.actorOf(Props[ToActor], name = randomUUID().toString)
  val toActorStop = context.actorOf(Props[ToActor], name = randomUUID().toString)
  val toActorEscalate = context.actorOf(Props[ToActor], name = randomUUID().toString)

  override def supervisorStrategy: SupervisorStrategy = OneForOneStrategy(
    maxNrOfRetries = 2,
    withinTimeRange = 2 minutes,
    loggingEnabled = true){
      case ResumeException(e) => {
        println(self.path.name + " received " + e)
        Resume
      }
      case RestartException(e) => {
        println(self.path.name + " received " + e)
        Restart
      }
      case StopException(e) => {
        println(self.path.name + " received " + e)
        Stop
      }
      case EscalateExcetption(e) => {
        println(self.path.name + " received " + e)
        Escalate
      }
  }


  override def receive: Receive = {
    case _ => {
      println("I do my thing")
      toActorResume ! Greeting("Resume")
      toActorRestart ! Greeting("Restart")
      toActorStop ! Greeting("Stop")
      toActorEscalate ! Greeting("Escalate")
    }
  }
}

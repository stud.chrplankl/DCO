package actor

import akka.actor.Actor

class ToActor extends Actor{
  def receive = {
    case Greeting("Restart") => {
      throw  new RestartException("please restart")
    }
    case Greeting("Resume") =>
      throw new ResumeException("please resume")
    case Greeting("Stop") =>
      throw new StopException("please stop")
    case Greeting("Escalate") =>
      throw new EscalateExcetption("please escalate")

    case p@Greeting(_) => {
      sender() ! Greeting(p.toString)
    }
  }
}

package actor

import akka.actor.Actor

class BecomeActor extends Actor{
  override def receive: Receive = {
    case "SwitchContext" => {
      println("switching to newReceive")
      context.become(newReceive)
      println("switched to newReceive")
    }
  }

  def newReceive: Receive = {
    case "SwitchContext" => {
      println("switching to receive")
      context.become(receive);
      println("switched to receive")
    }
  }
}

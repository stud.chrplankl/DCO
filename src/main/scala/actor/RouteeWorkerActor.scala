package actor

import akka.actor.{Actor, Props}

class RouteeWorkerActor extends Actor{

  override def receive: Receive = {
    case msg: Any => println("name:" + self.path.name +" message: " + msg.toString)
   }
}

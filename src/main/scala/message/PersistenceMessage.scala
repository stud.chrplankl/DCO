package message

sealed trait PersistenceMessage
sealed trait RecoveryMessage

sealed case class ProtocolMessage(msg: String) extends PersistenceMessage
sealed case class CommandMessage(msg: String) extends PersistenceMessage

sealed case class ProtocolReceivedMessage(msg: String) extends RecoveryMessage
sealed case class CommandReceivedMessage(msg: String)extends RecoveryMessage


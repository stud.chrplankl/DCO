import actor._

import scala.util.{Failure, Success, Try}
import akka.actor.{ActorRef, ActorSystem, Props}
import akka.pattern.ask
import akka.routing.{RoundRobinGroup, RoundRobinPool}
import akka.util.Timeout
import message.{CommandMessage, ProtocolMessage}

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._
import scala.io.StdIn

object System extends App {
  println("Starting up my first actor system")

  implicit val timeout = Timeout(5 seconds)
  implicit val ec: ExecutionContext = ExecutionContext.Implicits.global

  val system: ActorSystem = ActorSystem("my-actor-system")

  val becomeActor = system.actorOf(Props[BecomeActor], "becomeActor")

  becomeActor ! "SwitchContext"
  becomeActor ! "SwitchContext"

  println("done")
  StdIn.readLine()

}

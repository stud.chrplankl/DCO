import scala.util.{Failure, Success, Try}

def iterate(numbers: List[Any]) ={
  numbers match {
    case x :: y :: List(hd::tl) :: z => "matched"
    case _ => "default"
  }
}
val l2 = List(1,List(1,2),3)
val l = List(1,2, List(1,2), 3)
iterate(l)
iterate(l2)

def factorial(number: Int) : Int = {
  def factorialWithAccumulator(accumulator: Int, number: Int) : Int = {
    if (number == 1)
      return accumulator
    else
      factorialWithAccumulator(accumulator * number, number - 1)
  }
  factorialWithAccumulator(1, number)
}
println(factorial(5))

val	rivers	=	List("Danube",	"Mississippi",	"Rhine",	"Amazonas")

rivers.sortBy(x=> x.length)

rivers.sortBy(x=> x.length).take(2)

val	cities	=	List("New	York	(US)",	"Tempe	(US)",	"Regensburg	(DE)",	"Hamburg	(DE)",
  "Vienna	(AT)",	"Ohio	(US)")

cities.groupBy( city => city.substring(city.length-4))

val lengths = List(2860, 2320, 1230, 6400)

var zipped = rivers zip lengths

zipped.sortBy(x=>x._2)

rivers.flatMap(x=> x.toLowerCase.toCharArray).groupBy(x=>x).map(x=>(x._1, x._2.length))



rivers.flatMap(x=>x.toUpperCase.toCharArray).sorted.foldLeft("")( (x, y) => x + y )


val	imdb	=	Map(
  "Brad	Pitt"	->	List("Mr.	and	Mrs.	Smith",	"Meet	Joe	Black",	"Benjamin	Button"),
  "Angelina	Jolie"	->	List("Tomb	Raider",	"Mr.	and	Mrs.	Smith"),
  "Anthony	Hopkins"	->	List("Silence	of	the	Lambs",	"Meet	Joe	Black",
    "The	Human	Stain",	"Amistad")
)

def	getActorOfFilm(movies:	Map[String,	List[String]],	film:	String) = {
  movies.filter(x=>x._2.contains(film)).map(x=>x._1).headOption

  //Option(movies.filter( {case (actor, films) => films.contains(film) } ).head._1)
}

def getActorsOfFilm(movies: Map[String, List[String]], film: String) : Option[List[String]] = {
  val actors = List[String]()
  Option(
    movies.filter( {
      case (actor, films) if films.contains(film) => true
      case _ => false
    })
      .foldLeft( actors )( {
        case (accu, (actor, films)) => actor :: accu
      })
  )
}



getActorOfFilm(imdb, "Meet	Joe	Black")


getActorsOfFilm(imdb, "Meet	Joe	Black")


rivers foreach println


var myMap = Map(1-> "one", 2-> "two")

myMap.foreach(x=> println( x._1))



val oddsAndEvens = List(List(1,2,3,4,5,6), List(1,2,4,5,7,9))

oddsAndEvens.flatten


class Person(val s: String){
  def say(text: String)(p2: Person): Unit ={
    println(s + " say " + text + " to " + p2.s)
  }
}

val p1 = new Person("John")
val p2 = new Person("Jane")

p1.say("hi")(p2)

